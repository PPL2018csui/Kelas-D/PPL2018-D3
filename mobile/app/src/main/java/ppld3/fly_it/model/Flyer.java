package ppld3.fly_it.model;

import android.graphics.Bitmap;

/**
 * Created by USER on 3/8/2018.
 */

public class Flyer {

    protected String owner;
    protected String title;
    protected String description;
    protected Bitmap image;

    public Flyer(String owner, String title, String description, Bitmap image) {
        this.owner = owner;
        this.title = title;
        this.description = description;
        this.image = image;
    }

    public Flyer(String owner, String title, String description) {
        this.owner = owner;
        this.title = title;
        this.description = description;
        this.image = null;
    }

    public String getOwner() {
        return this.owner;
    }

    public String getDesc() {
        return this.description;
    }

    public Bitmap getImage() {
        return this.image;
    }

    public String getTitle() {
        return this.title;
    }
}
