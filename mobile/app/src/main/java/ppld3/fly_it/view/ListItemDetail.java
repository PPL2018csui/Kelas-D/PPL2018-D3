package ppld3.fly_it.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import ppld3.fly_it.R;

/**
 * Created by USER on 3/5/2018.
 */

public class ListItemDetail extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_itemdetail);

        Intent intent = getIntent();
        String owner = intent.getStringExtra("owner");
        String title = intent.getStringExtra("title");
        String desc = intent.getStringExtra("desc");
        //Bitmap image = intent.getParcelableExtra("image");
        try {
            byte[] byteArray = intent.getByteArrayExtra("image");
            Bitmap image = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            ImageView imageAds = (ImageView) findViewById(R.id.flyer_image);
            imageAds.setImageBitmap(image);
        } catch (Exception e) {
            ImageView imageAds = (ImageView) findViewById(R.id.flyer_image);
            imageAds.setVisibility(View.GONE);

        }

        TextView ownerText = (TextView) findViewById(R.id.ownerName);
        ownerText.setText(owner);
        TextView titletext = (TextView) findViewById(R.id.title);
        titletext.setText(title);
        TextView desctext = (TextView) findViewById(R.id.desc);
        desctext.setText(desc);
        desctext.setMovementMethod(new ScrollingMovementMethod());

    }

}
