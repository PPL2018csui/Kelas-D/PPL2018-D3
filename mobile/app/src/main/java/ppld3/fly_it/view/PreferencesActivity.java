package ppld3.fly_it.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import ppld3.fly_it.R;
import ppld3.fly_it.model.Flyer;

import static android.content.ContentValues.TAG;

/**
 * Created by USER on 4/15/2018.
 */

public class PreferencesActivity extends Activity {

    CheckBox ch1,ch2,ch3,ch4;
    Button pref;
    String prefs;
    FirebaseDatabase database;
    DatabaseReference myRef;
    private FirebaseAuth mAuth;
    String[] prefList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferences);

        ch1=(CheckBox)findViewById(R.id.checkBox1);
        ch2=(CheckBox)findViewById(R.id.checkBox2);
        ch3=(CheckBox)findViewById(R.id.checkBox3);
        ch4=(CheckBox)findViewById(R.id.checkBox4);
        pref=(Button)findViewById(R.id.buttonPref);

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("users").child(currentUser.getUid()).child("prefs");
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                prefs = dataSnapshot.getValue(String.class);
                prefList = prefs.split(",");
                for (int i = 0; i < prefList.length; i++) {
                    String mypref = prefList[i];
                    if (mypref.equals("1")) {
                        ch1.setChecked(true);
                    }
                    if (mypref.equals("2")) {
                        ch2.setChecked(true);
                    }
                    if (mypref.equals("3")) {
                        ch3.setChecked(true);
                    }
                    if (mypref.equals("4")) {
                        ch4.setChecked(true);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

        pref.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                prefs = "";
                if (ch1.isChecked()) {
                    prefs = prefs + "1,";
                }
                if (ch2.isChecked()) {
                    prefs = prefs + "2,";
                }
                if (ch3.isChecked()) {
                    prefs = prefs + "3,";
                }
                if (ch4.isChecked()) {
                    prefs = prefs + "4,";
                }
                if (prefs.length() > 1) {
                    prefs = prefs.substring(0,prefs.length()-1);
                }
                myRef.setValue(prefs);
                Intent intent = getIntent();
                if (intent.getStringExtra("from").equals("register")) {
                    Intent intent2 = new Intent();
                    intent2.setClass(getApplicationContext(), FlyerPage.class);
                    startActivity(intent2);
                }
                else {
                    finish();
                }

            }
        });

    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
