package ppld3.fly_it.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ppld3.fly_it.R;
import ppld3.fly_it.adapter.FlyerPageAdapter;
import ppld3.fly_it.math.Haversine;
import ppld3.fly_it.utils.DividerItemDecoration;
import ppld3.fly_it.utils.GPSTracker;
import ppld3.fly_it.utils.ImageDownload;
import ppld3.fly_it.utils.VerticalSpacingDecoration;
import ppld3.fly_it.model.Flyer;
import ppld3.fly_it.utils.HttpHandler;

import static android.content.ContentValues.TAG;

public class FlyerPage extends AppCompatActivity {

    private List<Flyer> flyerList = new ArrayList<>();
    private RecyclerView rv;
    private FlyerPageAdapter ca;
    private Bitmap myBitmap;
    private FirebaseAuth mAuth;
    FirebaseDatabase database;
    DatabaseReference myRef;
    String[] prefList;
    String prefs;
    FirebaseUser currentUser;
    private DrawerLayout mDrawerLayout;
    private MenuItem prefMenu;
    List<Address> addresses;

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser == null) {
            Intent intent = new Intent();
            intent.setClass(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                if(mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                }
                else {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flyer_page);

        mDrawerLayout = findViewById(R.id.drawer_layout);
        mDrawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerOpened(View arg0) {

            }

            @Override
            public void onDrawerClosed(View arg0) {

            }

            @Override
            public void onDrawerSlide(View arg0, float arg1) {
                if (prefMenu != null) {
                    prefMenu.setChecked(false);
                }
            }

            @Override
            public void onDrawerStateChanged(int arg0) {
                //write your code
            }
        });



        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);

        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();

        Intent intent = getIntent();

        rv = (RecyclerView) findViewById(R.id.recycler_view);
        rv.addItemDecoration(new DividerItemDecoration(ContextCompat.getDrawable(getApplicationContext(), R.drawable.item_decorator)));

        rv.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rv.setLayoutManager(llm);

        TextView email = (TextView) findViewById(R.id.email);
        email.setText("Welcome, "+currentUser.getDisplayName());

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        // set item as selected to persist highlight
                        menuItem.setChecked(true);
                        // close drawer when item is tapped
                        mDrawerLayout.closeDrawers();

                        int id = menuItem.getItemId();

                        if (id == R.id.nav_ref) {
                            finish();
                            startActivity(getIntent());
                        } else if (id == R.id.nav_pref) {
                            prefMenu = menuItem;
                            Intent intent = new Intent();
                            intent.setClass(getApplicationContext(), PreferencesActivity.class);
                            intent.putExtra("from", "home");
                            startActivity(intent);
                        } else if (id == R.id.nav_info) {
                            prefMenu = menuItem;

                        } else if (id == R.id.nav_out) {
                            mAuth.signOut();
                            Intent intent = new Intent();
                            intent.setClass(getApplicationContext(), LoginActivity.class);
                            startActivity(intent);
                        }

                        return false;
                    }
                });

        new GetContacts().execute();
    }

    private class GetContacts extends AsyncTask<Void, Void, Void> {
        double[] loc;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Toast.makeText(FlyerPage.this, "Json Data is downloading", Toast.LENGTH_LONG).show();
            database = FirebaseDatabase.getInstance();
            myRef = database.getReference("users").child(currentUser.getUid()).child("prefs");
            myRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    prefs = dataSnapshot.getValue(String.class);
                    prefList = prefs.split(",");
                }

                @Override
                public void onCancelled(DatabaseError error) {
                    // Failed to read value
                    Log.w(TAG, "Failed to read value.", error.toException());
                }
            });
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            final GPSTracker gps = new GPSTracker(FlyerPage.this);
            final Geocoder geocoder = new Geocoder(FlyerPage.this, Locale.getDefault());

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loc = gps.getLoc();

                    try {
                        addresses = geocoder.getFromLocation(loc[1], loc[0], 1);
                    } catch (final IOException e) {

                    }
                    String cityName = "lost. Turn on your GPS, then click Refresh.";
                    if (addresses.size() != 0) {
                        cityName = "at " + addresses.get(0).getAddressLine(0);
                    }

                    TextView longlat = (TextView) findViewById(R.id.longlat);
                    longlat.setText("You are "+cityName);
                }
            });

            Haversine hav = new Haversine();

            HttpHandler sh = new HttpHandler();
            // Making a request to url and getting response
            String url = "https://api.myjson.com/bins/1301id";
            final String jsonStr = sh.makeServiceCall(url);

            while (prefs == null) {}
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    JSONArray ads = jsonObj.getJSONArray("advertisements");

                    // looping through All Contacts
                    for (int i = 0; i < ads.length(); i++) {
                        JSONObject c = ads.getJSONObject(i);
                        String image = "";
                        String imageLink = c.getString("image");
                        if(!imageLink.equalsIgnoreCase("")){
                            image = imageLink;
                        }
                        String owner = c.getString("author");
                        String title = c.getString("title");
                        String desc = c.getString("description");

                        Double longi = Double.parseDouble(c.getString("long"));
                        Double lat = Double.parseDouble(c.getString("lat"));

                        if(!imageLink.equalsIgnoreCase("")){
                            ImageDownload imgDown = new ImageDownload();
                            myBitmap = imgDown.getBitmapFromURL(image);
                            if (myBitmap.getWidth() > 600 || myBitmap.getHeight() > 600) {
                                myBitmap = imgDown.getResizedBitmap(myBitmap, 600);
                            }
                        }
                        Boolean tagOke = false;
                        try {
                            String tags = c.getString("tag");
                            for (int ii = 0; ii < prefList.length; ii++) {
                                String mypref = prefList[ii];
                                if (mypref.equals(tags)) {
                                    tagOke = true;
                                    break;
                                }
                            }
                        } catch (Exception e) {
                            tagOke = true;
                        }

                        if (tagOke && hav.isInRadius(loc[0],loc[1],longi,lat,500000)) {
                            if(image.equalsIgnoreCase("")){
                                flyerList.add(new Flyer(owner, title, desc));
                            }
                            else{
                                flyerList.add(new Flyer(owner, title, desc, myBitmap));
                            }
                        }

                    }
                    ImageDownload imgDown = new ImageDownload();
                    myBitmap = imgDown.getBitmapFromURL("https://i.imgur.com/HbTP3BG.jpg");
                    if (myBitmap.getWidth() > 600 || myBitmap.getHeight() > 600) {
                        myBitmap = imgDown.getResizedBitmap(myBitmap, 600);
                    }
                    flyerList.add(new Flyer("Adityo's magical store", "Promo Dunkin Donut Buy" +
                            " 12 Free 12 Only for student", "aaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaahehe",myBitmap));

                } catch (final JSONException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
                    });
                    ImageDownload imgDown = new ImageDownload();
                    myBitmap = imgDown.getBitmapFromURL("https://i.imgur.com/HbTP3BG.jpg");
                    if (myBitmap.getWidth() > 600 || myBitmap.getHeight() > 600) {
                        myBitmap = imgDown.getResizedBitmap(myBitmap, 600);
                    }
                    flyerList.add(new Flyer("Adityo's magical store", "Promo Dunkin Donut Buy" +
                            " 12 Free 12 Only for student", "aaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaahehe",myBitmap));
                    flyerList.add(new Flyer("Yo bro", "Promo spa gratis", "aaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaahehe"));
                }

            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG).show();
                    }
                });
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            ca = new FlyerPageAdapter(flyerList, new CustomItemClickListener() {
                @Override
                public void onItemClick(View v, int position) {
                    //Log.d(TAG, "clicked position:" + position);
                    String owner = flyerList.get(position).getOwner();
                    String title = flyerList.get(position).getTitle();
                    String desc = flyerList.get(position).getDesc();
                    //Toast.makeText(getApplicationContext(), title, Toast.LENGTH_LONG).show();
                    Intent intent = new Intent();
                    intent.setClass(getApplicationContext(), ListItemDetail.class);
                    intent.putExtra("owner", owner);
                    intent.putExtra("title", title);
                    intent.putExtra("desc", desc);
                    try {
                        Bitmap image = flyerList.get(position).getImage();
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        image.compress(Bitmap.CompressFormat.PNG, 100, stream);
                        byte[] byteArray = stream.toByteArray();
                        intent.putExtra("image", byteArray);
                    } catch (NullPointerException e) {

                    }
                    startActivity(intent);
                    // do what ever you want to do with it
                }
            });
            rv.setAdapter(ca);
            rv.addItemDecoration(new VerticalSpacingDecoration(24));
        }
    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}

