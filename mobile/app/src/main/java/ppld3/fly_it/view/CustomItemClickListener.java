package ppld3.fly_it.view;

import android.view.View;

/**
 * Created by USER on 3/8/2018.
 */

public interface CustomItemClickListener {
    public void onItemClick(View v, int position);
}
