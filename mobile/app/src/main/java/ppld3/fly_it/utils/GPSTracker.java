package ppld3.fly_it.utils;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.lang.Math;

import java.util.List;

/**
 * Created by USER on 3/19/2018.
 */

public class GPSTracker extends Service implements LocationListener{

    private final Context mContext;
    private static final int MY_PERMISSION_ACCESS_COARSE_LOCATION = 11;
    private static final int MY_PERMISSION_ACCESS_FINE_LOCATION = 12;
    LocationManager lm;
    public GPSTracker(Context context) {
        this.mContext = context;
        lm = (LocationManager) mContext.getSystemService(mContext.LOCATION_SERVICE);
    }

    public double[] getLoc() {
        //LocationManager lm = (LocationManager) mContext.getSystemService(mContext.LOCATION_SERVICE);
        if ( ContextCompat.checkSelfPermission( (Activity) mContext, android.Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED ) {

            ActivityCompat.requestPermissions( (Activity) mContext, new String[] {  android.Manifest.permission.ACCESS_COARSE_LOCATION  },
                    this.MY_PERMISSION_ACCESS_COARSE_LOCATION );
        }
        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER,1000*60*30,100,this);
        List<String> providers = lm.getProviders(true);

        Location l = null;

        for (int i=providers.size()-1; i>=0; i--) {
            try {
                l = lm.getLastKnownLocation(providers.get(i));
            } catch (SecurityException e) {

            }
            //l = lm.getLastKnownLocation(providers.get(i));
            if (l != null) break;
        }

        double[] gps = new double[2];
        if (l != null) {
            gps[0] = l.getLongitude();
            gps[1] = l.getLatitude();
        }
        return gps;
    }
    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onLocationChanged(Location location) {
        //lm.removeUpdates(this);
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

}
