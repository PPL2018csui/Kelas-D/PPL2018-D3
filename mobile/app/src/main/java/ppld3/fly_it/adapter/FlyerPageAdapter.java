package ppld3.fly_it.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

import ppld3.fly_it.R;
import ppld3.fly_it.model.Flyer;
import ppld3.fly_it.view.CustomItemClickListener;

/**
 * Created by USER on 3/8/2018.
 */

public class FlyerPageAdapter extends RecyclerView.Adapter<FlyerPageAdapter.MyViewHolder> {

    private List<Flyer> flyerList;
    CustomItemClickListener listener;


    /**
     * View holder class
     * */
    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public CardView flyerCard;
        public TextView ownerText;
        public TextView titleText;
        //public TextView descText;
        public ImageView flyerImage;

        public MyViewHolder(View view) {
            super(view);
            flyerCard = (CardView) view.findViewById(R.id.iklan);
            ownerText = (TextView) view.findViewById(R.id.ownerName);
            titleText = (TextView) view.findViewById(R.id.title);
            //descText = (TextView) view.findViewById(R.id.desc);
            flyerImage = (ImageView) view.findViewById(R.id.flyer_image);
        }

    }

    public FlyerPageAdapter(List<Flyer> flyerList, CustomItemClickListener listener) {
        this.flyerList = flyerList;
        this.listener = listener;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        System.out.println("Bind ["+holder+"] - Pos ["+position+"]");
        Flyer c = flyerList.get(position);
        holder.ownerText.setText(c.getOwner());
        holder.titleText.setText(c.getTitle());
        /*if (c.getDesc().length() < 100) {
            holder.descText.setText(String.valueOf(c.getDesc().substring(0, c.getDesc().length())));
        }
        else {
            holder.descText.setText(String.valueOf(c.getDesc().substring(0, 100)+"..."));
        }*/
        holder.flyerImage.setImageBitmap(c.getImage());
        if (c.getImage() == null) {
            holder.flyerImage.setVisibility(View.GONE);
        }
        else {
            holder.flyerImage.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        Log.d("RV", "Item size ["+flyerList.size()+"]");
        return flyerList.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_list,parent, false);
        final MyViewHolder mv = new MyViewHolder(v);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, mv.getAdapterPosition());
            }
        });
        return mv;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
