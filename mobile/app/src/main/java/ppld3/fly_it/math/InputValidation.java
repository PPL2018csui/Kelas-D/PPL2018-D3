package ppld3.fly_it.math;

public class InputValidation {
    public InputValidation () {

    }

    public boolean isPassMatch(String pass1, String pass2){
        if(pass1.equals(pass2)){
            return true;
        }else{
            return false;
        }
    }

    public boolean isLoginFull(String email, String pass) {
        if(email.isEmpty() || pass.isEmpty()) {
            return false;
        }
        else {
            return true;
        }

    }

    public boolean isRegFull(String nama, String email, String pass1, String pass2) {
        if(nama.isEmpty() || email.isEmpty() || pass1.isEmpty() || pass2.isEmpty()) {
            return false;
        }
        else {
            return true;
        }

    }

}
