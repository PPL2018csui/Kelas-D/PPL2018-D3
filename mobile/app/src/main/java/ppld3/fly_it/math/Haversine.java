package ppld3.fly_it.math;

/**
 * Created by USER on 3/21/2018.
 */

public class Haversine {

    public Haversine () {

    }

    public double distance(double lon1, double lat1, double lon2, double lat2) {

        final double R = 6372.8; // Radius of the earth

        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);

        double a = Math.pow(Math.sin(dLat / 2),2) + Math.pow(Math.sin(dLon / 2),2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.asin(Math.sqrt(a));
        return R * c * 1000;

    }

    public boolean isInRadius(double lon1, double lat1, double lon2, double lat2, double radius) {

        double distance = this.distance(lon1, lat1, lon2, lat2);
        if (distance <= radius) {
            return true;
        } else {
            return false;
        }
    }
}
