package ppld3.fly_it;

import org.junit.Before;
import org.junit.Test;

import ppld3.fly_it.math.InputValidation;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ValidationTest {

    private InputValidation calc;

    @Before
    public void setUp() throws Exception {
        calc = new InputValidation();
    }

    @Test
    public void test_validation1() throws Exception {
        assertFalse(calc.isPassMatch("secretpass","publicpass"));
    }

    @Test
    public void test_validation2() throws Exception {
        assertFalse(calc.isPassMatch("truepass","Truepass"));
    }

    @Test
    public void test_validation3() throws Exception {
        assertTrue(calc.isPassMatch("truepass","truepass"));
    }

    @Test
    public void test_validation4() throws Exception {
        assertTrue(calc.isRegFull("aa","bb","cc","dd"));
    }

    @Test
    public void test_validation5() throws Exception {
        assertFalse(calc.isRegFull("aa","bb","","dd"));
    }

    @Test
    public void test_validation6() throws Exception {
        assertTrue(calc.isLoginFull("truepass","truepass"));
    }

    @Test
    public void test_validation7() throws Exception {
        assertFalse(calc.isLoginFull("truepass",""));
    }

}