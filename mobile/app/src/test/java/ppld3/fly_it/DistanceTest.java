package ppld3.fly_it;

import org.junit.Test;

import org.junit.Before;
import org.mockito.Mockito;


import ppld3.fly_it.math.Haversine;
import ppld3.fly_it.utils.GPSTracker;
import ppld3.fly_it.view.FlyerPage;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class DistanceTest {

    private Haversine calc;

    @Before
    public void setUp() throws Exception {
        calc = new Haversine();
    }

    @Test
    public void test_distance1() throws Exception {
        assertFalse(calc.isInRadius(-6.359476, 106.819381, -6.25947, 106.71662, 500));
    }

    @Test
    public void test_distance2() throws Exception {
        assertTrue(calc.isInRadius(-6.359476, 106.819381, -6.35986, 106.82007, 500));
    }

}