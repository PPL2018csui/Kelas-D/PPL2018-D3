# Fly-It. A Project by PPL-D3. Supports system development of localized and personalized advertisements platform. In collaboration with Halonesia.

[![Dependency Status](https://gemnasium.com/gitlabhq/gitlabhq.svg)](https://gemnasium.com/gitlabhq/gitlabhq)
[![Code Climate](https://codeclimate.com/github/gitlabhq/gitlabhq.svg)](https://codeclimate.com/github/gitlabhq/gitlabhq)
[![Core Infrastructure Initiative Best Practices](https://bestpractices.coreinfrastructure.org/projects/42/badge)](https://bestpractices.coreinfrastructure.org/projects/42)
[![Gitter](https://badges.gitter.im/gitlabhq/gitlabhq.svg)](https://gitter.im/gitlabhq/gitlabhq?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)

## Pipeline
- [![build status](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/badges/v0.5/build.svg)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/commits/v0.5) (v0.5)
- [![build status](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/badges/master/build.svg)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/commits/master) (master)
- [![build status](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/badges/sit_uat/build.svg)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/commits/sit_uat) (sit_uat)
- [![build status](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/badges/coba_coba/build.svg)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/commits/coba_coba) (coba_coba)
- [![build status](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/badges/1-as-a-merchant-i-would-like-to-login-register-on-web-based-application/build.svg)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/commits/1-as-a-merchant-i-would-like-to-login-register-on-web-based-application) (1-Merchant-Login-Register)
- [![build status](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/badges/2-as-a-merchant-i-would-like-to-post-broadcast-advertisement-to-customers/build.svg)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/commits/2-as-a-merchant-i-would-like-to-post-broadcast-advertisement-to-customers) (2-Merchant-Broadcast-Advertisements)
- [![build status](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/badges/3-as-a-customer-i-d-like-to-receive-advertisements-from-merchants/build.svg)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/commits/3-as-a-customer-i-d-like-to-receive-advertisements-from-merchants) (3-Customer-Receive-Advertisements)
- [![build status](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/badges/4-as-a-merchant-i-d-like-to-broadcast-advertisements-to-specific-area/build.svg)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/commits/4-as-a-merchant-i-d-like-to-broadcast-advertisements-to-specific-area) (4-Localized-Advertisements)
- [![build status](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/badges/5-Mobile-Login-Register/build.svg)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/commits/5-Mobile-Login-Register) (5-Mobile-Login-Register)
- [![build status](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/badges/6-Customer-Preferences/build.svg)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/commits/6-Customer-Preferences) (6-Customer-Preferences)
- [![build status](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/badges/7-Merchant-Profile/build.svg)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/commits/7-Merchant-Profile) (7-Merchant-Profile)
- [![build status](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/badges/8-Advertisement-Statistics/build.svg)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/commits/8-Advertisement-Statistics) (8-Advertisement-Statistics)

## Test coverage

- [![Python coverage](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/badges/v0.5/coverage.svg?job=website)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/commits/v0.5) Python (v0.5)
- [![Coverage report](https://gitlab.com/gitlab-org/gitlab-ce/badges/v0.5/coverage.svg?job=coverage)](https://ppl2018csui.gitlab.io/Kelas-D/PPL2018-D3/commits/v0.5) Java (v0.5)
- [![Python coverage](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/badges/master/coverage.svg?job=website)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/commits/master) Python (master)
- [![Coverage report](https://gitlab.com/gitlab-org/gitlab-ce/badges/master/coverage.svg?job=coverage)](https://ppl2018csui.gitlab.io/Kelas-D/PPL2018-D3/commits/master) Java (master)
- [![Python coverage](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/badges/sit_uat/coverage.svg?job=website)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/commits/sit_uat) Python (sit_uat)
- [![Coverage report](https://gitlab.com/gitlab-org/gitlab-ce/badges/sit_uat/coverage.svg?job=coverage)](https://ppl2018csui.gitlab.io/Kelas-D/PPL2018-D3/commits/sit_uat) Java (sit_uat)
- [![Python coverage](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/badges/coba_coba/coverage.svg?job=test)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/commits/coba_coba) Python (coba_coba)
- [![Coverage report](https://gitlab.com/gitlab-org/gitlab-ce/badges/coba_coba/coverage.svg?job=coverage)](https://ppl2018csui.gitlab.io/Kelas-D/PPL2018-D3/commits/coba_coba) Java (coba_coba)
- [![Python coverage](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/badges/1-as-a-merchant-i-would-like-to-login-register-on-web-based-application/coverage.svg?job=website)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/commits/1-as-a-merchant-i-would-like-to-login-register-on-web-based-application) Python (1-Merchant-Login-Register)
- [![Coverage report](https://gitlab.com/gitlab-org/gitlab-ce/badges/1-as-a-merchant-i-would-like-to-login-register-on-web-based-application/coverage.svg?job=coverage)](https://ppl2018csui.gitlab.io/Kelas-D/PPL2018-D3/commits/1-as-a-merchant-i-would-like-to-login-register-on-web-based-application) Java (1-Merchant-Login-Register)
- [![Python coverage](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/badges/2-as-a-merchant-i-would-like-to-post-broadcast-advertisement-to-customers/coverage.svg?job=website)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/commits/2-as-a-merchant-i-would-like-to-post-broadcast-advertisement-to-customers) Python (2-Merchant-Broadcast-Advertisements)
- [![Coverage report](https://gitlab.com/gitlab-org/gitlab-ce/badges/2-as-a-merchant-i-would-like-to-post-broadcast-advertisement-to-customers/coverage.svg?job=coverage)](https://ppl2018csui.gitlab.io/Kelas-D/PPL2018-D3/commits/2-as-a-merchant-i-would-like-to-post-broadcast-advertisement-to-customers) Java (2-Merchant-Broadcast-Advertisements)
- [![Python coverage](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/badges/3-as-a-customer-i-d-like-to-receive-advertisements-from-merchants/coverage.svg?job=test)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/commits/3-as-a-customer-i-d-like-to-receive-advertisements-from-merchants) Python (3-Customer-Receive-Advertisements)
- [![Coverage report](https://gitlab.com/gitlab-org/gitlab-ce/badges/3-as-a-customer-i-d-like-to-receive-advertisements-from-merchants/coverage.svg?job=coverage)](https://ppl2018csui.gitlab.io/Kelas-D/PPL2018-D3/commits/3-as-a-customer-i-d-like-to-receive-advertisements-from-merchants) Java (3-Customer-Receive-Advertisements)
- [![Python coverage](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/badges/4-as-a-merchant-i-d-like-to-broadcast-advertisements-to-specific-area/coverage.svg?job=website)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/commits/4-as-a-merchant-i-d-like-to-broadcast-advertisements-to-specific-area) Python (4-Localized-Advertisements)
- [![Coverage report](https://gitlab.com/gitlab-org/gitlab-ce/badges/4-as-a-merchant-i-d-like-to-broadcast-advertisements-to-specific-area/coverage.svg?job=coverage)](https://ppl2018csui.gitlab.io/Kelas-D/PPL2018-D3/commits/4-as-a-merchant-i-d-like-to-broadcast-advertisements-to-specific-area) Java (4-Localized-Advertisements)
- [![Python coverage](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/badges/5-Mobile-Login-Register/coverage.svg?job=website)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/commits/5-Mobile-Login-Register) Python (5-Mobile-Login-Register)
- [![Coverage report](https://gitlab.com/gitlab-org/gitlab-ce/badges/5-Mobile-Login-Register/coverage.svg?job=coverage)](https://ppl2018csui.gitlab.io/Kelas-D/PPL2018-D3/commits/5-Mobile-Login-Register) Java (5-Mobile-Login-Register)
- [![Python coverage](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/badges/6-Customer-Preferences/coverage.svg?job=website)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/commits/6-Customer-Preferences) Python (6-Customer-Preferences)
- [![Coverage report](https://gitlab.com/gitlab-org/gitlab-ce/badges/6-Customer-Preferences/coverage.svg?job=coverage)](https://ppl2018csui.gitlab.io/Kelas-D/PPL2018-D3/commits/6-Customer-Preferences) Java (6-Customer-Preferences)
- [![Python coverage](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/badges/7-Merchant-Profile/coverage.svg?job=website)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/commits/7-Merchant-Profile) Python (7-Merchant-Profile)
- [![Coverage report](https://gitlab.com/gitlab-org/gitlab-ce/badges/7-Merchant-Profile/coverage.svg?job=coverage)](https://ppl2018csui.gitlab.io/Kelas-D/PPL2018-D3/commits/7-Merchant-Profile) Java (7-Merchant-Profile)
- [![Python coverage](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/badges/8-Advertisement-Statistics/coverage.svg?job=website)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D3/commits/8-Advertisement-Statistics) Python (8-Advertisement-Statistics)
- [![Coverage report](https://gitlab.com/gitlab-org/gitlab-ce/badges/8-Advertisement-Statistics/coverage.svg?job=coverage)](https://ppl2018csui.gitlab.io/Kelas-D/PPL2018-D3/commits/8-Advertisement-Statistics) Java (8-Advertisement-Statistics)

## How the Applications Work

There are two applications in this repository, web-application which is based on Django Python Framework and mobile-application which is based on Java Native Android Application. The web-application is created for the merchant while the mobile-application is created for the customers. The general steps are as follows:

1. Merchant register in the web-application.
2. Merchant in the web-application broadcasts the advertisements to the customers in the mobile-application.
3. Customer receive the advertisement from the web-application in the mobile-application.

More detailed information can be found [here](https://halonesia.co.id).

Main Focus:
* Broadcast Advertising to specific area.
* Broadcast Advertising to personalised users. 

## Getting Started (Development Guide)

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

Term & Conditions:
1. Master branch must be sterilize, supervisor respondsible, team do not merge to master branch. 
2. Every work in a user story, must be in one branch starts with number digit, '-' sign and user short story name. For example: 1-mapping (for first user story, Mapping data).
3. If creating sub-branch inside the user story, when merge, please delete the sub-branch to simplify the repository.
4. If user story has been completed, please one people (usually the last one completed :D) merge the branch-sit_uat for SIT (software integration testing) & UAT (user acceptance test) when sprint review. 
5. If when the sprint review decided to accept, then supervisor (concurrently as the git master) will be merging to branch master.

### Prerequisites (How to set up your machine)

1. Navigate to the directory where you've cloned this repo and setting up the environment variable first.
2. Still in the directory where you've cloned this repo, install all its dependencies.

    ```bash
    pip3 install -r website/requirements.txt
    ```

    Dependencies are all listed in `website/requirements.txt`. To re-generate
    this file (after you've installed new packages), simply run
    `pip3 freeze > website/requirements.txt`. For Linux users, if you have a
    problem installing the dependencies, install `python3-dev` or
    `python3-devel` system package first.

3. Run the app

    ```bash
    python3 website/manage.py runserver
    ```
6. The app is now running! To check that the web is actually running,
try to send a GET request to it, for instance:

    ```bash
    curl http://127.0.0.1:5432
    ```

    or open `http://localhost:5432/myapp/hello` from your browser. You should get a
    response that says:

    ```bash
    Hello world
    ```

### Installing (How to run the tests/linters)

1. Make sure you already installed [pytest][pytest] and [flake8][flake8].
Both are listed in `website/requirements.txt` so if you followed the instructions
to setup your machine above then they should already be installed.
2. You can run the migration with `python3 website/manage.py makemigrations` and
`python3 website/manage.py migrate` respectively.
3. To run both linters and tests in one command, you can use
`python3 website/manage.py check`. This is useful to check your code before making
a merge request.
4. For more info on what you can do with `manage.py`, run
`python3 website/manage.py --help`.

[pytest]: http://pytest.org/latest/
[flake8]: https://pypi.python.org/pypi/flake8

## Deployment

* [Test](https://localhost:5432/myapp/hello) - Branch coba_coba
* [Staging](https://halonesia.herokuapp.com) - Branch SAT_UAT
* [Production](http://flyit.arsyady.com) - Branch Master 

## Built With

* [Django](https://www.djangoproject.com/) - The web framework used in backend (Python)
* [Android Studio](https://developer.android.com/studio/index.html?hl=id) - Native Android Mobile-Application IDE (Java)
* [PostgreSQL](https://www.postgresql.org/) - Used to generate database

## Contributing (How to Contribute)

If you want to write new features to this application or fix bugs, that's great! Here is a step-by-step guide to contribute to this application's development.

1. You need an issue on GitLab about your contribution. This can be a
bug report (in which case your contribution is the bug fix) or feature
suggestion (in which case your contribution is the implementation).
2. Make sure that you are in `sit_uat` branch by running `git status`.
If not, run `git checkout sit_uat` to move to `sit_uat` branch.
3. Create a new branch on which you write your code. Use branch
name satisfy the terms & conditions above. For example, `1-mapping`:

    ```bash
    git checkout -b 1-mapping
    ```

4. Implement your contribution in the branch.
5. Periodically, and after you committed your changes, pull the latest
changes from sit_uat and merge the changes to your working branch. This
ensures your branch is always up-to-date with the origin.

    ```bash
    git pull origin sit_uat
    git merge sit_uat
    ```

    Fix any conflicts that may arise.
6. After you really finished writing the code, commit your changes. You
may create one or more commits.
7. Push the feature branch to `origin`:

    ```bash
    git push origin 1-mapping
    ```

8. Create a new merge request on GitLab for `1-mapping` branch to be
merged to `sit_uat`. Assign the supervisor as the assignee of the
new merge request.
9. Wait for other project members and instructors (owner) to review your
contribution.
10. If they approve your contribution, congrats! Your contribution may
be merged to sit_uat. 
11. (Optional) After your contribution is merged, you may safely delete your feature branch:

    ```bash
    git branch -D 1-mapping
    ```

    Also delete your feature branch on origin from Gitlab.

    ```bash
    git push origin :1-mapping
    ```

12. Done!

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

Developer Team:
- Adit 
- Ardian
- Bryanza
- Citra
- Eldy
- Warman

Supervisor: Ferdinand

* **Adityawarman Fanaro** - *Hipster* - [GitLab](https://gitlab.com/reznov53)
* **Adityo Anggraito** - *Hacker* - [GitLab](https://gitlab.com/primetime49)
* **Ardian Jati Permadi** - *Hacker* - [GitLab](https://gitlab.com/ardianjatip)
* **Bryanza Novirahman** - *Hacker* - [GitLab](https://gitlab.com/bryanzanr)
* **Citra Glory** - *Hustler* - [GitLab](https://gitlab.com/citraglory)
* **Eldy Hidayat** - *Hipster* - [GitLab](https://gitlab.com/eldy.hidayat)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Important links
* [Medium Blog](https://medium.com/fly-it)
* [Lean Canvas](https://drive.google.com/open?id=1BlJP0uTbYBl6lf55oLk8uyu0IQKTfxyGv84lOtsZf2o)
* [Mockup](https://drive.google.com/open?id=1EsiiJfDM-VSlsGxuoMSAGZvFPW3Sw3cs)
Program Used : Balsamiq (https://balsamiq.com/)
* [Project Vision](https://drive.google.com/open?id=13TiOJ2GBeG5oJHvYutd9WQiiyMLH96Mcdv-g1jJ5rHc)
* [Overview Presentation](https://drive.google.com/open?id=1OLfYYgj1g2ni2pl8_i-HQavOnJgcIm8bX-dSCP5kLlc)
* [Mobile Merchant Mockup](https://marvelapp.com/13g3c3fg/)
* [Customer Mockup](https://marvelapp.com/4f8049f)
* [Desktop Merchant Mockup](https://marvelapp.com/7ij9ie1)
* [Product Backlog Item](https://drive.google.com/open?id=1wJJfNYHFSehBmpTYCRS0YrJoV5TQvJJalE2fZEMppGg)
* [Halonesia](https://halonesia.co.id)
* [fly-it Staging](https://halonesia.herokuapp.com)
* [fly-it Production](http://flyit.arsyady.com)
* [Sprint 1 Review](https://drive.google.com/open?id=1ahu6Qr96wZorZNiSs3Dixgx1qZmcyBnLLgQYGO8D2eQ)
* [UI/UX Guidelines](https://drive.google.com/open?id=1wqjC-pzVwjH7o0KdJyMkTgl6rZPZjIU-UMuRR6RTKfA)